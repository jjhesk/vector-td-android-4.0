package se.par.amsen.vectortd.main.fragment;

import java.util.List;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateFadeOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceFadeIn;
import se.par.amsen.vectortd.game.score.ScoreItem;
import se.par.amsen.vectortd.game.score.ScoreService;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ScoresMenuFragment extends ControlledFragment {
	private static final String TAG = "OptionsMenuFragment";

	private Button btnBack;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(R.layout.fragment_main_scores, container, false);

		OptionsOnClickListener listener = new OptionsOnClickListener();

		btnBack = (Button) v.findViewById(R.id.btn_back);
		btnBack.setOnClickListener(listener);

		ListView scoreView = (ListView) v.findViewById(R.id.scores_list);

		ScoreArrayAdapter adapter = new ScoreArrayAdapter(getActivity(), ScoreService.INSTANCE.getScores());

		scoreView.setAdapter(adapter);

		return v;
	}

	@Override
	public void transitionInSetup(final AnimatorListenerAdapter listener) {

		setTransitionIn(new TransitionBounceFadeIn(listener, R.color.red_transp));
	}

	@Override
	public void transitionOut(final AnimatorListenerAdapter listener) {
		TransitionAccelerateFadeOut transition = new TransitionAccelerateFadeOut(listener, R.color.red_transp);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnBack.performClick();
	}

	private class ScoreArrayAdapter extends ArrayAdapter<ScoreItem> {
		private final Context context;
		private final List<ScoreItem> values;

		public ScoreArrayAdapter(Context context, List<ScoreItem> values) {
			super(context, R.layout.row_scores_list, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.row_scores_list, parent, false);
			TextView num = (TextView) rowView.findViewById(R.id.score_num);
			TextView name = (TextView) rowView.findViewById(R.id.score_name);

			num.setText(String.valueOf(values.get(position).getScore()));
			name.setText(String.valueOf(values.get(position).getName()));

			return rowView;
		}
	}

	private class OptionsOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.SCORES_MENU)) {
				int id = v.getId();
				if(id == btnBack.getId()) {
					getFragmentController().swap(FragmentControllerService.INSTANCE.SCORES_MENU,
							FragmentControllerService.INSTANCE.MAIN_MENU, null);
				}
			}
		}
	}
}
