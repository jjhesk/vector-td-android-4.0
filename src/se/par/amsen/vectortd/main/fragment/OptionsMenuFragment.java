package se.par.amsen.vectortd.main.fragment;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateFadeOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceFadeIn;
import se.par.amsen.vectortd.game.audio.AudioService;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class OptionsMenuFragment extends ControlledFragment {
	private static final String TAG = "OptionsMenuFragment";

	Button btnBack;

	Context context;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(R.layout.fragment_main_options, container, false);
		context = v.getContext();

		OptionsOnClickListener listener = new OptionsOnClickListener();

		btnBack = (Button) v.findViewById(R.id.btn_back);
		btnBack.setOnClickListener(listener);

		SeekBar music = (SeekBar) v.findViewById(R.id.seekbar_music_vol);
		music.setProgress((int) (AudioService.INSTANCE.getMusicVolume()*100));

		SeekBar audio = (SeekBar) v.findViewById(R.id.seekbar_aud_vol);
		audio.setProgress((int) (AudioService.INSTANCE.getAudioVolume()*100));

		music.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser) {
					AudioService.INSTANCE.setMusicVolume((float)progress/100);
				}
			}
		});

		audio.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				AudioService.INSTANCE.setAudioVolume((float)seekBar.getProgress()/100);

				//play sound so user hears current volume
				AudioService.INSTANCE.play(AudioService.ID_SFX_ENEMY_DIE);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { } 

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { }
		});

		return v;
	}

	@Override
	public void transitionInSetup(AnimatorListenerAdapter listener) {
		setTransitionIn(new TransitionBounceFadeIn(listener, R.color.red_transp));
	}

	@Override
	public void transitionOut(AnimatorListenerAdapter listener) {
		TransitionAccelerateFadeOut transition = new TransitionAccelerateFadeOut(listener, R.color.red_transp);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnBack.performClick();
	}

	private class OptionsOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.OPTIONS_MENU)) {
				int id = v.getId();
				if(id == btnBack.getId()) {
					AudioService.INSTANCE.saveSettingsToFile(context);

					getFragmentController().swap(FragmentControllerService.INSTANCE.OPTIONS_MENU,
							FragmentControllerService.INSTANCE.MAIN_MENU, null);
				}
			}
		}
	}


}
