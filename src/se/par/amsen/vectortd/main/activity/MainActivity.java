package se.par.amsen.vectortd.main.activity;

import java.util.List;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentController;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.game.audio.AudioService;
import se.par.amsen.vectortd.game.fragment.GameFragment;
import se.par.amsen.vectortd.game.fragment.GameUiFragment;
import se.par.amsen.vectortd.game.fragment.HighScoreFragment;
import se.par.amsen.vectortd.game.fragment.PauseFragment;
import se.par.amsen.vectortd.game.fragment.SmsFragment;
import se.par.amsen.vectortd.game.score.ScoreService;
import se.par.amsen.vectortd.main.fragment.MainMenuFragment;
import se.par.amsen.vectortd.main.fragment.OptionsMenuFragment;
import se.par.amsen.vectortd.main.fragment.ScoresMenuFragment;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	public static final String TAG = "MainActivity";

	FragmentController controller;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        init();
        
        setContentView(R.layout.container);
        
        ControlledFragment mainFragment = new MainMenuFragment();
        controller.load(FragmentControllerService.INSTANCE.MAIN_MENU, mainFragment);
        controller.push(FragmentControllerService.INSTANCE.MAIN_MENU);

        ControlledFragment optionsFragment = new OptionsMenuFragment();
        ControlledFragment scoresFragment = new ScoresMenuFragment();
        
        ControlledFragment gamePlayFragment = new GameFragment();
        ControlledFragment gamePauseFragment = new PauseFragment();
        ControlledFragment gameUiFragment = new GameUiFragment();
        ControlledFragment gameHighScoreFragment = new HighScoreFragment();
        ControlledFragment gameSmsFragment = new SmsFragment();
        
        controller.load(FragmentControllerService.INSTANCE.OPTIONS_MENU, optionsFragment);
        controller.load(FragmentControllerService.INSTANCE.SCORES_MENU, scoresFragment);
        
        controller.load(FragmentControllerService.INSTANCE.GAME_PLAY, gamePlayFragment);
        controller.load(FragmentControllerService.INSTANCE.GAME_PAUSE, gamePauseFragment);
        controller.load(FragmentControllerService.INSTANCE.GAME_UI, gameUiFragment);
        controller.load(FragmentControllerService.INSTANCE.GAME_HIGH_SCORE, gameHighScoreFragment);
        controller.load(FragmentControllerService.INSTANCE.GAME_SMS, gameSmsFragment);
    }
    
    @Override
    public void onBackPressed() {
    	if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.MAIN_MENU)) {
    		finish(); //exit app if user press back in main activity
    	} else {
        	FragmentControllerService.INSTANCE.getFragmentController().onBackPressed();
    	}

    }
    
    @Override
    public void onAttachFragment(Fragment fragment) {
    	List<ControlledFragment> fragments = FragmentControllerService.INSTANCE.getFragmentController().getFragmentsAsList();
    	
    	if(!fragments.contains(fragment)) {
    		getFragmentManager().beginTransaction().remove(fragment).commit();
    		Log.w(TAG, "Removed conflicting fragment!");
    	}
    }
    
    public void init() {
    	FragmentControllerService.INSTANCE.
    	setFragmentController(new FragmentController(R.id.container, getFragmentManager()));
    	controller = FragmentControllerService.INSTANCE.getFragmentController();
    	ScoreService.INSTANCE.init(this);
    	AudioService.INSTANCE.init(this);
    	AudioService.INSTANCE.play(AudioService.ID_MAIN_MUSIC); 
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	AudioService.INSTANCE.pause(AudioService.ID_MAIN_MUSIC);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	AudioService.INSTANCE.play(AudioService.ID_MAIN_MUSIC);
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	AudioService.INSTANCE.releaseResources();
    }
}
