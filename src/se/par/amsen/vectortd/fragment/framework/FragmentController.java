package se.par.amsen.vectortd.fragment.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import se.par.amsen.vectortd.fragment.transition.ControlledFragmentTransitionListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.util.Log;

public class FragmentController {
	
	private static final String TAG = "FragmentController";
	
	private Map<String, ControlledFragment> controlledFragments;
	private int containerViewId;
	private FragmentManager fragmentManager;
	
	private Map<String, String> switchOperation;
	
	private List<String> fragmentHistory;

	public FragmentController(int containerViewId, FragmentManager fragmentManager) {
		switchOperation = new ConcurrentHashMap<String, String>();
		fragmentHistory = new ArrayList<String>();
		
		controlledFragments = new HashMap<String, ControlledFragment>();
		this.fragmentManager = fragmentManager;
		this.containerViewId = containerViewId;
	}

	public void load(String tag, ControlledFragment fragment) {
		controlledFragments.put(tag, fragment);
	}

	public void unload(String tag) {
		controlledFragments.remove(tag);
	}

	public void pushWithTransition(final String tag, final ControlledFragmentTransitionListener listener) {
		final ControlledFragment fragment = controlledFragments.get(tag);

		push(tag);

		fragment.transitionInSetup(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				if(listener != null) {
					listener.onTransitionStart();
				}
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				if(listener != null) {
					listener.onTransitionEnd();
				}
			}
		});
	}

	public void push(String tag) {
		ControlledFragment fragment = controlledFragments.get(tag);
		
		if(fragment.isAdded()) {
			Log.w(TAG, "Fragment is already added...");
			return;
		}

		fragmentManager.beginTransaction().
		add(containerViewId, fragment, tag).
		commit();
		
		fragmentHistory.add(tag);
	}

	public void removeAfterTransition(final String tag, final ControlledFragmentTransitionListener listener) {
		final ControlledFragment fragment = controlledFragments.get(tag);
		
		fragment.transitionOut(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				if(listener != null) {
					listener.onTransitionStart();
				}
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				if(listener != null) {
					listener.onTransitionEnd();
				}
				remove(tag);
			}
		});
	}

	public void remove(String tag) {
		ControlledFragment fragment = controlledFragments.get(tag);
		
		if(!fragment.isAdded()) {
			Log.w(TAG, "Fragment to be removed is not added...");
			return;
		}
		
		fragmentManager.beginTransaction().
		remove(fragment).
		commit();
		
		fragmentHistory.remove(tag);
	}

	public synchronized void swap(final String from, final String to, final ControlledFragmentTransitionListener listener) {
		if(switchOperation.get(from) != null) {
			Log.w(TAG, "Switch already in action!");
			return;
		}
		
		switchOperation.put(from,to);
		
		final ControlledFragment fragmentFrom = controlledFragments.get(from);
		final ControlledFragment fragmentTo = controlledFragments.get(to);

		fragmentFrom.transitionOut(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				if(listener != null) {
					listener.onTransitionStart();
				}
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				remove(from);
				push(to);

				fragmentTo.transitionInSetup(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						switchOperation.remove(from);
						
						if(listener != null) {
							listener.onTransitionEnd();
						}
					}
				});
			}
		});
	}
	
	public void onBackPressed() {
		controlledFragments.get(getFocus()).onBackPressed();
	}

	public String getFocus() {
		return fragmentHistory.get(fragmentHistory.size()-1);
	}

	public List<ControlledFragment> getFragmentsAsList() {
		return new ArrayList<ControlledFragment>(controlledFragments.values());
	}
}
