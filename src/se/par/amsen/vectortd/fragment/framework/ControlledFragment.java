package se.par.amsen.vectortd.fragment.framework;

import se.par.amsen.vectortd.fragment.transition.animator.TransitionAnimator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class ControlledFragment extends Fragment {
	private FragmentController controller;
	
	private TransitionAnimator transitionIn;
	
	public ControlledFragment() {
		controller = FragmentControllerService.INSTANCE.getFragmentController();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = init(inflater, container);
		
		if(getTransitionIn() != null){
			getTransitionIn().load(v).start();
		}

		return v;
	}
	
	public abstract View init(LayoutInflater inflater, ViewGroup container);
	
	public abstract void transitionInSetup(AnimatorListenerAdapter listener);
	
	public abstract void transitionOut(AnimatorListenerAdapter listener);
	
	public abstract void onBackPressed();
	
	public void hideView() {
		getView().setAlpha(0f);
	}
	
	public void showView() {
		getView().setAlpha(1.0f);
	}

	public FragmentController getFragmentController() {
		return controller;
	}

	public void setFragmentController(FragmentController parent) {
		this.controller = parent;
	}

	public TransitionAnimator getTransitionIn() {
		return transitionIn;
	}

	public void setTransitionIn(TransitionAnimator transitionIn) {
		this.transitionIn = transitionIn;
	}
		
}
