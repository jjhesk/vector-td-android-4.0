package se.par.amsen.vectortd.fragment.transition;

public abstract class ControlledFragmentTransitionListener {
	public abstract void onTransitionEnd();
	public abstract void onTransitionStart();
}