package se.par.amsen.vectortd.view;

import se.par.amsen.vectortd.font.FontService;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {
	public FontTextView(Context context) {
		super(context);
		init(context);
	}
	
	public FontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public FontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public void init(Context context) {
		setTypeface(FontService.getInstance(context).getTypeFace());
	}
}
