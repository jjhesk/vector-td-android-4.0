package se.par.amsen.vectortd.network;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

public class NetworkService {
	private Context context;

	public NetworkService(Context context){
		this.context = context;
	}

	public boolean isConnectedToInternet(){
		ConnectivityManager connectivityManager  = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

		return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
	}

	public void openWirelessSettings(){
		Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
		context.startActivity(intent);
	}
}