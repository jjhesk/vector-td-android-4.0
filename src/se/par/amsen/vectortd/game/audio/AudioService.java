package se.par.amsen.vectortd.game.audio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import se.par.amsen.vectortd.R;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;
import android.util.SparseIntArray;

public enum AudioService {
	INSTANCE;

	public final static String TAG = "AudioService";

	public final static int ID_MAIN_MUSIC = 1;

	private String basePath;
	private final static String FILE_NAME = "audioSettings.dat";

	private SparseIntArray sfx;
	public final static int ID_SFX_ENEMY_DIE = 2;

	private float musicVolume;
	private float audioVolume;

	private SoundPool pool;

	private MediaPlayer musicPlayer;

	private void loadSettingsFromFile(final Context context) {
		File baseDir = new File(basePath);

		if(!baseDir.exists()) {
			initializeDefault();
			saveSettingsToFile(context);
			return;
		}
		
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.v(TAG, "Attempting to load audio settings from file");
				StringBuilder stringBuilder = new StringBuilder();
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader( context.openFileInput(FILE_NAME)));

					String temp;

					while ((temp = br.readLine()) != null) {
						stringBuilder.append(temp);
					} 
					br.close();
				} catch (IOException e) {
					Log.e(TAG, "Exception loading audio settings from file");
					return;
				}

				String unParsed = stringBuilder.toString().replaceAll(",", ".");
				
				String[] parsed = unParsed.split(":");

				musicVolume = Float.valueOf(parsed[0]); 
				audioVolume = Float.valueOf(parsed[1]); 
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}

	public void saveSettingsToFile(final Context context) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.v(TAG, "Attempting to save audio settings to file");
				File baseDir = new File(basePath);
				if(!baseDir.exists()) {
					try {
						baseDir.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
						return;
					}
				}

				try {
					FileOutputStream outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
					outputStream.write(String.format("%.2f:%.2f", musicVolume, audioVolume).getBytes());
					outputStream.close();
				} catch (IOException e) {
					Log.e(TAG, "Exception audio saving settings to file");
					return;
				} 
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}

	private void initializeDefault() {
		Log.v(TAG, "set to default audio settings");
		musicVolume = .75f;
		audioVolume = .9f;
	}

	private void load(Context context) {
		sfx.put(ID_SFX_ENEMY_DIE, pool.load(context, R.raw.enemy_die, 1));
	}

	public void init(Context context) {	
		basePath = context.getFilesDir().getPath()  + "/" + FILE_NAME;
		loadSettingsFromFile(context);

		sfx = new SparseIntArray(10);

		pool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);		

		musicPlayer = MediaPlayer.create(context, R.raw.chiptune_does_dubstep);
		musicPlayer.setLooping(true);
		musicPlayer.setVolume(musicVolume, musicVolume);
		musicPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

		load(context);
	}

	public void play(int id) {
		switch(id) {
		case ID_MAIN_MUSIC:
			musicPlayer.start();
			break;
		case ID_SFX_ENEMY_DIE:
			pool.play(sfx.get(ID_SFX_ENEMY_DIE), audioVolume, audioVolume, 1, 0, 1);
		}
	}

	public void pause(int id) { 
		switch(id) {
		case ID_MAIN_MUSIC:
			musicPlayer.pause();
			break;

		}
	}

	public void stop(int id) {
		switch(id) {
		case ID_MAIN_MUSIC:
			musicPlayer.stop();
			break;
		}
	}

	public void setMusicVolume(float value) {
		musicVolume = value >= .1f ? value : .1f ;
		musicVolume /= 7; //because sounds are too loud!
		
		musicPlayer.setVolume(musicVolume, musicVolume);
	}

	public void setAudioVolume(float value) {
		audioVolume = value >= .1f ? value : .1f; 
		audioVolume /= 7; //because sounds are too loud!
	}

	public float getMusicVolume() { 
		return musicVolume;
	}

	public float getAudioVolume() {
		return audioVolume;
	}

	public void releaseResources() {
		musicPlayer.release();
		pool.release();
	}
}