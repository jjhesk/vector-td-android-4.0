package se.par.amsen.vectortd.game.score;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.util.Log;

public enum ScoreService {
	INSTANCE;

	private final static String TAG = "ScoreService";

	private String basePath;
	private final static String FILE_NAME = "scores.dat";

	private List<ScoreItem> scores;

	/**
	 * Initialize scores, will load if possible, else
	 * initialize scores with default values
	 */
	public void init(Context context) {
		basePath = context.getFilesDir().getPath()  + "/" + FILE_NAME;
		loadScoresFromFile(context);
	}

	/**
	 * Generates the default values into the scores
	 */
	private void initializeDefault() {
		scores = new ArrayList<ScoreItem>();
		scores.add(new ScoreItem(500, "Udo"));
		scores.add(new ScoreItem(400, "Linnea"));
		scores.add(new ScoreItem(300, "Lena"));
		scores.add(new ScoreItem(200, "P�r"));
		scores.add(new ScoreItem(100, "Fool Jr."));
	}

	/**
	 * Load scores from file.
	 */
	public void loadScoresFromFile(final Context context) {		
		File baseDir = new File(basePath);

		if(!baseDir.exists()) {
			initializeDefault();
			saveScoresToFile(context);
			return;
		}

		scores = new ArrayList<ScoreItem>();

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.v(TAG, "load scores from file");
				StringBuilder stringBuilder = new StringBuilder();
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader( context.openFileInput(FILE_NAME)));

					String temp;

					while ((temp = br.readLine()) != null) {
						stringBuilder.append(temp);
					} 
					br.close();
				} catch (IOException e) {
					Log.e(TAG, "Exception loading scores from file");
					return;
				}

				String[] parsed = stringBuilder.toString().split("#"); 

				for(String scoreItem : parsed) {
					if(scoreItem.length() != 0) {
						String[] split = scoreItem.split(":");
						scores.add(new ScoreItem(Integer.valueOf(split[0]), split[1]));
					}
				}
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}

	/**
	 * Save scores to file.
	 */
	public void saveScoresToFile(final Context context) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.v(TAG, "save scores to file");
				File baseDir = new File(basePath);
				if(!baseDir.exists()) {
					try {
						baseDir.createNewFile();
					} catch (IOException e) {
						Log.e(TAG, "Exception saving scores to file");
						return;
					}
				}

				try { 
					FileOutputStream outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);

					StringBuilder sb = new StringBuilder();

					for (int i = 0 ; i < scores.size() ; i++) {
						sb.append(String.format("%d:%s%s", scores.get(i).getScore(), scores.get(i).getName(), 
								i!=scores.size()-1 ? "#" : "")); 
					}

					outputStream.write(sb.toString().getBytes());
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				} 
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();
	}

	/**
	 * Calculates the rank of the score in comparison to the other scores
	 * Grabbed from my old Snake project!
	 * @param score int score value
	 * @return the rank (0-4)
	 */
	public int calculateRank(int score) {
		int rank = -1;
		for(int counter = scores.size() - 1 ; counter >= 0 ; counter--){
			if( score > scores.get(counter).getScore()){
				rank = counter;
			}
		}
		return rank;
	}

	/**
	 * Update the scores
	 * @param name String player name or null for default value
	 * @param score int score value
	 */
	public void update(String name, int score) {
		int rank = calculateRank(score);

		if( rank != -1 ) {

		}
	}

	public List<ScoreItem> getScores() {
		return scores;
	}
}
