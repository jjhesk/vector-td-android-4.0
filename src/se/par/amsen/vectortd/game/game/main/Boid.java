package se.par.amsen.vectortd.game.game.main;

import se.par.amsen.vector2D.main.Vector2D;
public class Boid {
	private double maxForce = 100;
	private double maxVelocity = 20;

	private double x;
	private double y;

	private Vector2D position;
	private Vector2D velocity;
	private Vector2D target;
	private Vector2D desired;
	private Vector2D steering;
	private double mass;

	private int windowWidth;
	private int windowHeight;

	private GameWorld gameWorld;

	public Boid(double posX, double posY, double mass, Vector2D target, int windowWidth, int windowHeight, GameWorld gameWorld) {
		this.position = new Vector2D(posX, posY);
		this.velocity = new Vector2D((100*Math.random())-50, (100*Math.random())-50);
		this.target = target;
		this.mass = mass;

		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		this.gameWorld = gameWorld;

		velocity.normalize();

		maxVelocity = 1 + maxVelocity * Math.random();
		maxForce = maxVelocity * 20;
		truncate(velocity, maxVelocity);
	}

	public void truncate(Vector2D vector, double max) {
		double i = max / vector.getLength();

		i = i < 1.0 ? 1.0 : i;

		vector.scaleBy(i);
	}

	public void update() {	
		if(gameWorld.getTime() % 60 == 1) {
			target.setX(windowWidth*Math.random());
			target.setY(windowHeight*Math.random());
		}

		Vector2D steering = seek();
		Vector2D avoid = avoid();
		steering.add(avoid);

		if(steering.getLength() != 0) {

			truncate(steering, maxForce);
			steering.scaleBy(1/mass);

			velocity.add(steering);

		}

		truncate(velocity, 1/mass);
		position.add(velocity);

		x = position.getX();
		y = position.getY();

		this.steering = steering;
	}

	private Vector2D avoid() {
		Vector2D force = new Vector2D(0,0);

		for (Boid boid : gameWorld.getBoids()) {
			if (position.distance(boid.getPosition()) < 200) {
				force = position.copy();
				force.subtract(boid.position);
			}
		}
		
		if(force.getLength() != 0) {
			force.normalize();
			truncate(force,maxVelocity);
		}
		
		return force;
	}

	private Vector2D seek() {
		Vector2D force = new Vector2D(0,0);

		desired = target.copy();
		desired.subtract(position);

		desired.normalize();
		desired.scaleBy(maxVelocity);

		if(desired.distance(velocity) < 0.5) {
			return new Vector2D(0,0);
		}

		force = desired.copy();
		force.subtract(velocity);

		return force;
	}


	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Vector2D getPosition() {
		return position;
	}

	public void setPosition(Vector2D position) {
		this.position = position;
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}

	public Vector2D getDesired() {
		return desired;
	}

	public void setDesired(Vector2D desired) {
		this.desired = desired;
	}

	public Vector2D getSteering() {
		return steering;
	}

	public void setSteering(Vector2D steering) {
		this.steering = steering;
	}
}