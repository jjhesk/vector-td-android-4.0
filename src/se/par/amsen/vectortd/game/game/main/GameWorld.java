package se.par.amsen.vectortd.game.game.main;

import java.util.ArrayList;
import java.util.List;

import se.par.amsen.vector2D.main.Vector2D;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.View;

public class GameWorld {

	public static final String TAG = "GameWorld";

	private Context context;

	private List<Boid> boids;

	RenderView renderView;

	int windowWidth;
	int windowHeight;
	
	double radius;

	public GameWorld(Context context, int windowWidth, int windowHeight) {
		this.context = context;
		renderView = new RenderView(context);
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;

		boids = new ArrayList<Boid>();
		
		radius = context.getResources().getDisplayMetrics().density;
		radius *= windowWidth/300;
	}

	public void init() {
		for (int i = 0; i < 40; i++) {
			boids.add(new Boid(windowWidth * Math.random(), windowHeight * Math.random(), 500 + Math.random() * 50, new Vector2D(windowWidth * Math.random(), windowHeight * Math.random()), windowWidth, windowHeight, this));
		}
	}

	public void start() {
		renderView.start();
	}

	public void pause() {
		renderView.stop();
	}

	public void stop() {
		renderView.stop();
	}

	public void reset() {
		renderView.stop();

		boids.clear();

		init();

		renderView.start();
	}

	private void update(Canvas canvas) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setStrokeWidth(5);

		for (Boid boid : boids) {
			boid.update();

			paint.setColor(Color.RED);
			paint.setStyle(Style.FILL);

			canvas.drawCircle((int)boid.getX()-3, (int)boid.getY()-3, (int)radius, paint);

			paint.setColor(Color.WHITE);
			paint.setStyle(Style.STROKE);
			canvas.drawCircle((int)boid.getX()-3, (int)boid.getY()-3, (int)radius, paint);

			drawForceVector(canvas, boid.getPosition().copy(), boid.getDesired().copy(), Color.GREEN, 80);
			drawForceVector(canvas, boid.getPosition().copy(), boid.getVelocity().copy(), Color.BLUE, 80);
		}
	}

	private void drawForceVector(Canvas canvas, Vector2D from, Vector2D to, int color, double scale) {
		Paint paint = new Paint();
		paint.setColor(color);

		paint.setAntiAlias(true);
		paint.setStrokeWidth(3);
		paint.setStyle(Style.STROKE);

		to.normalize();
		to.scaleBy(scale);	
		canvas.drawLine((int)from.getX(), (int)from.getY(), (int)from.getX() + (int)to.getX(), (int)from.getY() + (int)to.getY(), paint);
	}


	public RenderView getRenderView() {
		return renderView;
	}

	public void setRenderView(RenderView renderView) {
		this.renderView = renderView;
	}

	public int getTime() {
		return renderView.getTime();
	}

	public List<Boid> getBoids() {
		return boids;
	}

	public void setBoids(List<Boid> boids) {
		this.boids = boids;
	}


	private class RenderView extends View implements Runnable{
		private int time = 0;
		private volatile boolean running = false;

		private Thread renderThread = null;

		public RenderView(Context context) {
			super(context);
		}

		public void start() {
			Log.v(TAG, "Attempt to start gameloop thread...");

			running = true;

			renderThread = new Thread(this);

			renderThread.start();
		}

		public void stop() {
			running = false;

			Log.v(TAG, "Attempt to stop gameloop thread...");

			try {
				renderThread.join();
			} catch (InterruptedException e) {
				Log.w(TAG, "Gameloop thread interrupted!");
			}

			Log.v(TAG, "Gameloop thread successfully stopped...");
		}

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			update(canvas);
		}

		@Override
		public void run() {
			while(running) {
				try {
					postInvalidate();
					Thread.sleep(30);
					time++;
				} catch (InterruptedException e) {
					Log.w(TAG, "Gameloop thread interrupted");
				}
			}
		}
		public int getTime() {

			return time;
		}
	}
}
