package se.par.amsen.vectortd.game.game.main;

import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import android.content.Context;

public enum GameService {
	INSTANCE;

	private GameWorld gameWorld;
	private Context context;

	public void init(Context context, int windowWidth, int windowHeight) {
		gameWorld = new GameWorld(context, windowWidth, windowHeight);
		gameWorld.init();
		
		this.context = context;
	}

	public void start() {
		gameWorld.start();
	}
	
	public void addUI() {
		FragmentControllerService.INSTANCE.getFragmentController().
		pushWithTransition(FragmentControllerService.INSTANCE.GAME_UI, null);
	}

	public void pause() {
		gameWorld.pause();
	}

	public void stop() {
		gameWorld.stop();
	}
	
	public void reset() {
		gameWorld.reset();
	}

	public GameWorld getGameWorld() {
		return gameWorld;
	}

	public void setGameWorld(GameWorld gameWorld) {
		this.gameWorld = gameWorld;
	}

	public int getTime() {
		return gameWorld.getTime();
	}
	
	
}
