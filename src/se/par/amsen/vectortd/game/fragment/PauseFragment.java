package se.par.amsen.vectortd.game.fragment;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.ControlledFragmentTransitionListenerAdapter;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateFadeOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceFadeIn;
import se.par.amsen.vectortd.game.game.main.GameService;
import android.animation.AnimatorListenerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class PauseFragment extends ControlledFragment {
	private static final String TAG = "PauseFragment";

	private Button btnResumeGame;
	private Button btnExitGame;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		View v = inflater.inflate(R.layout.fragment_game_pause, container, false);

		OptionsOnClickListener listener = new OptionsOnClickListener();

		btnResumeGame = (Button) v.findViewById(R.id.btn_resume_game);
		btnResumeGame.setOnClickListener(listener);

		btnExitGame = (Button) v.findViewById(R.id.btn_exit_game);
		btnExitGame.setOnClickListener(listener);

		return v;
	}

	@Override
	public void transitionInSetup(final AnimatorListenerAdapter listener) {

		setTransitionIn(new TransitionBounceFadeIn(listener, R.color.blue_transp));
	}

	@Override
	public void transitionOut(final AnimatorListenerAdapter listener) {
		TransitionAccelerateFadeOut transition = new TransitionAccelerateFadeOut(listener, R.color.blue_transp);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnExitGame.performClick();
	}

	private class OptionsOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.GAME_PAUSE)) {
				int id = v.getId();
				if(id == btnResumeGame.getId()) {
					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_PAUSE, null);
					GameService.INSTANCE.start();
				} else if(id == btnExitGame.getId()) {
					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_PAUSE, new ControlledFragmentTransitionListenerAdapter() {					
						@Override
						public void onTransitionEnd() {
							getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_UI,  new ControlledFragmentTransitionListenerAdapter() {
								@Override
								public void onTransitionEnd() {
									getFragmentController().swap(FragmentControllerService.INSTANCE.GAME_PLAY, FragmentControllerService.INSTANCE.MAIN_MENU,  null);
									GameService.INSTANCE.stop();
								}
							});

						}
					});
				}
			}
		}
	}


}