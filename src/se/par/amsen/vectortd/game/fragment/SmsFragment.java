package se.par.amsen.vectortd.game.fragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import se.par.amsen.vectortd.R;
import se.par.amsen.vectortd.fragment.framework.ControlledFragment;
import se.par.amsen.vectortd.fragment.framework.FragmentControllerService;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionAccelerateFadeOut;
import se.par.amsen.vectortd.fragment.transition.animator.TransitionBounceFadeIn;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;

public class SmsFragment extends ControlledFragment {
	private static final String TAG = "SMSMenuFragment";

	private Button btnBack;
	private Button btnSpam;

	private View v;

	Set<Contact> spam;

	@Override
	public View init(LayoutInflater inflater, ViewGroup container) {
		v = inflater.inflate(R.layout.fragment_game_sms, container, false);

		spam = new HashSet<Contact>();

		SmsOnClickListener listener = new SmsOnClickListener();

		btnBack = (Button) v.findViewById(R.id.btn_back);
		btnSpam = (Button) v.findViewById(R.id.btn_spam);

		btnBack.setOnClickListener(listener);
		btnSpam.setOnClickListener(listener);

		RetrieveContactsTask task = new RetrieveContactsTask();

		task.execute(new Void[0]);

		return v;
	}

	@Override
	public void transitionInSetup(final AnimatorListenerAdapter listener) {

		setTransitionIn(new TransitionBounceFadeIn(listener, R.color.blue_transp));
	}

	@Override
	public void transitionOut(final AnimatorListenerAdapter listener) {
		TransitionAccelerateFadeOut transition = new TransitionAccelerateFadeOut(listener, R.color.blue_transp);
		transition.load(getView()).start();
	}
	
	@Override
	public void onBackPressed() {
		btnBack.performClick();
	}

	private class ContactsArrayAdapter extends ArrayAdapter<Contact> {
		private final Context context;
		private final List<Contact> values;

		public ContactsArrayAdapter(Context context, List<Contact> values) {
			super(context, R.layout.row_contacts_list, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public View getView(final int position, View v, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View rowView = inflater.inflate(R.layout.row_contacts_list, parent, false);
			CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox_contacts);
			TextView name = (TextView) rowView.findViewById(R.id.checkBox_text);

			name.setText(String.valueOf(values.get(position).getName()));

			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked) {
						Log.v(TAG, String.format("Name: %s | Num: %s | Selected", 
								values.get(position).getName(), values.get(position).getNumber()));

						spam.add(values.get(position));
					} else {
						Log.v(TAG, String.format("Name: %s | Num: %s | Deselected", 
								values.get(position).getName(), values.get(position).getNumber()));

						spam.remove(values.get(position));
					}
				}
			});

			return rowView;
		}
	}

	private class RetrieveContactsTask extends AsyncTask<Void, Void, Void> {

		private ListView contactsView;
		private ContactsArrayAdapter adapter;

		@Override
		protected void onPreExecute() {
			contactsView = (ListView) v.findViewById(R.id.contacts_list);
		};

		@Override
		protected Void doInBackground(Void... vd) {
			List<Contact> contacts = new ArrayList<Contact>();

			Cursor c = contactsView.getContext().getContentResolver().query( 
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, Phone.DISPLAY_NAME + " ASC"); 
			if(c.moveToFirst()) {
				do {

					int numberColumn  = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);   
					int nameColumn  = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

					contacts.add(new Contact(c.getString(nameColumn), c.getString(numberColumn)));
				} while(c.moveToNext());
			}
			c.close();
			adapter = new ContactsArrayAdapter(getActivity(), contacts);

			return null; 
		}

		@Override
		protected void onPostExecute(Void result) {
			contactsView.setAdapter(adapter);
		}

	}


	private class SmsOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if(FragmentControllerService.INSTANCE.getFragmentController().getFocus().equals(FragmentControllerService.INSTANCE.GAME_SMS)) {
				int id = v.getId();
				if(id == btnBack.getId()) {
					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_SMS, null);
				} else if(id == btnSpam.getId()) {
					SpamSMSTask spamTask = new SpamSMSTask();
					spamTask.execute(new Void[0]);

					Toast toast = Toast.makeText(getActivity(), "Spamming all your friends!", Toast.LENGTH_SHORT);
					toast.getView().setBackgroundResource(R.drawable.btn_red);
					toast.show();

					getFragmentController().removeAfterTransition(FragmentControllerService.INSTANCE.GAME_SMS, null);
				}
			}
		}

	}

	private class SpamSMSTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			for(Contact contact : spam) {
				Log.v(TAG, String.format("Spamming: %s, %s", contact.getName(), contact.getNumber()));

				/*Potentially dangerous code, commented out!*/

				//SmsManager smsManager = SmsManager.getDefault();
				//smsManager.sendTextMessage(contact.number, null, "I am having so much fun with VectorTD, play this very much fun game now!", null, null);
			}

			return null;
		}

	}

	private class Contact {
		private String name;
		private String number;

		public Contact(String name, String number) {
			this.number = number;
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}

	}


}
