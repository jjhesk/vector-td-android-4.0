package se.par.amsen.vector2D.main;

/**
 * Basic geometric 2D vector
 * @author Par Amsen
 *
 */
public class Vector2D {
	private double x;
	private double y;
	
	public Vector2D(double x, double y) {
		setX(x);
		setY(y);
	}
	
	public void add(Vector2D other) {
		x += other.getX();
		y += other.getY();
	}
	
	public void subtract(Vector2D other) {
		x -= other.getX();
		y -= other.getY();
	}
	
	public double distance(Vector2D other) {
		double x  = this.x - other.getX();
		double y = this.y - other.getY();

		return Math.sqrt(x*x+y*y);
	}
	
	public void scaleBy(double scalar) {
		x *= scalar;
		y *= scalar;
	}
	
	public void negate() {
		x = -x;
		y = -y;
	}
	
	public void normalize() {
		double length = getLength();
		
		x /= length;
		y /= length;
	}
	
	public Vector2D copy() {
		return new Vector2D(x,y);
	}
	
	public double getLength() {
		return Math.sqrt(x*x + y*y);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return String.format("[%f:%f]", getX(), getY());
	}
}
